package main

import (
	"bytes"
	"context"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/creack/pty"
)

func generate(ch chan<- string) {
	for {
		b, err := os.ReadFile("/df_linux/artifact_descriptions.dat")
		if err != nil && !os.IsNotExist(err) {
			log.Fatalln("Reading artifact descriptions:", err)
		}

		_ = os.Remove("/df_linux/artifact_descriptions.dat")
		_ = os.Remove("/df_linux/stderr.log")
		_ = os.Remove("/df_linux/gamelog.txt")
		_ = os.Remove("/df_linux/errorlog.txt")
		_ = os.RemoveAll("/df_linux/data/save/")

		ready := bytes.SplitAfter(b, []byte{0})

		if len(ready) > 0 && !bytes.HasSuffix(ready[len(ready)-1], []byte{0}) {
			ready = ready[:len(ready)-1]
		}

		log.Println("loaded artifact descriptions:", len(ready))

		for _, text := range ready {
			s := string(text[:len(text)-1]) // remove null byte

			start := strings.Index(s, "\n\n")
			comma := strings.Index(s, ", ")
			repeated := "This is" + s[comma+1:start] + ". "

			if strings.HasPrefix(s[start+2:], repeated) {
				s = s[:start+2] + s[start+2+len(repeated):]
			}

			const masterwork = "All craftsmanship is of the highest quality. "

			if strings.HasPrefix(s[start+2:], masterwork) {
				s = s[:start+2] + s[start+2+len(masterwork):]
			}

			if utf8.RuneCountInString(s) > *flagMaxLength {
				log.Println("discarding", utf8.RuneCountInString(s), "rune description")

				continue
			}

			ch <- s
		}

		log.Println("generating new descriptions")

		start := time.Now()

		runGame()

		log.Println("generating descriptions took", time.Since(start))
	}
}

func runGame() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Hour)
	defer cancel()

	cmd := exec.CommandContext(ctx, "/df_linux/dfhack", "+dfart")
	cmd.Env = append(cmd.Env, "TERM=xterm-256color")
	cmd.Dir = "/df_linux"

	f, err := pty.Start(cmd)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = cmd.Wait()
	if err != nil {
		panic(err)
	}
}
