package main

import (
	"context"
	"flag"
	"log"
	"time"

	"github.com/mattn/go-mastodon"
)

var (
	flagMaxLength    = flag.Int("max-length", 500, "maximum length of posts (runes)")
	flagKeepBuffer   = flag.Int("keep-buffer", 10, "minimum number of posts to have readied")
	flagPostAt       = flag.Duration("post-at", time.Minute*0xDF, "time of day to post (UTC)")
	flagServer       = flag.String("server", "https://mastodon.example", "Mastodon server to post on")
	flagClientKey    = flag.String("client-key", "", "OAuth2 client ID")
	flagClientSecret = flag.String("client-secret", "", "OAuth2 client secret")
	flagAccessToken  = flag.String("access-token", "", "OAuth2 access token")
)

func main() {
	flag.Parse()

	if *flagClientKey == "" || *flagClientSecret == "" || *flagAccessToken == "" {
		log.Fatal("missing OAuth2 flags")
	}

	if *flagMaxLength <= 0 {
		log.Fatal("maximum length must be positive")
	}

	if *flagKeepBuffer <= 0 {
		log.Fatal("minimum number of posts to have readied must be positive")
	}

	if *flagPostAt < 0 || *flagPostAt >= 24*time.Hour {
		log.Fatal("time of day to post must be in range [0, 23h59m]")
	}

	client := mastodon.NewClient(&mastodon.Config{
		Server:       *flagServer,
		ClientID:     *flagClientKey,
		ClientSecret: *flagClientSecret,
		AccessToken:  *flagAccessToken,
	})

	client.UserAgent = "ArtDwarvesMake/0.0.1"

	ch := make(chan string, *flagKeepBuffer-1)

	go generate(ch)

	nextPost := time.Now().UTC().Truncate(24 * time.Hour).Add(*flagPostAt)
	if time.Now().After(nextPost) {
		nextPost = nextPost.AddDate(0, 0, 1)
	}

	for {
		until := time.Until(nextPost)

		log.Println("next post in", until)
		time.Sleep(until)

		makePost(client, <-ch)

		nextPost = nextPost.AddDate(0, 0, 1)
	}
}

func makePost(client *mastodon.Client, text string) {
	_, err := client.PostStatus(context.Background(), &mastodon.Toot{
		Visibility: mastodon.VisibilityUnlisted,
		Status:     text,
	})
	if err != nil {
		log.Println("posting status:", err)
	}
}
