local screen = dfhack.gui.getCurViewscreen(true)

-- dismiss intro
while df.viewscreen_movieplayerst:is_instance(screen) do
	screen:feed_key(df.interface_key.LEAVESCREEN)
	screen = dfhack.gui.getCurViewscreen(true)
end

while screen.menu_line_id[screen.sel_menu_line] ~= df.viewscreen_titlest.T_menu_line_id.NewWorld do
	screen:feed_key(df.interface_key.STANDARDSCROLL_DOWN)
end

screen:feed_key(df.interface_key.SELECT)

screen = dfhack.gui.getCurViewscreen(true)

while screen.load_world_params do
	screen:logic()
end

if screen.welcome_msg ~= "" then
	screen:feed_key(df.interface_key.LEAVESCREEN)
end

screen:feed_key(df.interface_key.MENU_CONFIRM)

while #df.global.world.entities.all == 0 or screen.simple_mode or df.global.world.worldgen_status.state ~= 10 do
	screen:logic()
end

print("generated world")

screen:feed_key(df.interface_key.SELECT)

screen = dfhack.gui.getCurViewscreen(true)
while not df.viewscreen_titlest:is_instance(screen) do
	screen:logic()
	screen = dfhack.gui.getCurViewscreen(true)
end

-- start legends mode
screen:feed_key(df.interface_key.SELECT)
screen:feed_key(df.interface_key.STANDARDSCROLL_UP)
screen:feed_key(df.interface_key.SELECT)

-- if we didn't start legends mode, we selected the last of multiple worlds
screen = dfhack.gui.getCurViewscreen(true)
if df.viewscreen_titlest:is_instance(screen) then
	screen:feed_key(df.interface_key.STANDARDSCROLL_UP)
	screen:feed_key(df.interface_key.SELECT)
end

print("loading legends mode")

screen = dfhack.gui.getCurViewscreen(true)
while not df.viewscreen_legendsst:is_instance(screen) do
	screen:logic()
	screen = dfhack.gui.getCurViewscreen(true)
end

screen = dfhack.gui.getCurViewscreen(true)

while screen.init_step ~= -1 do
	screen:logic()
	screen:render()
end

print("checking " .. #screen.artifacts .. " artifacts for images")

local output = io.open("artifact_descriptions.dat", "ab")

local function format_text(text)
	text = dfhack.df2utf(text)
	text = text:gsub("%[P%]", "\n\n")
	text = text:gsub("%[[BR]%]", "\n")
	text = text:gsub("%[C:.:.:.%]", "")

	return text
end

for _,id in ipairs(screen.artifacts) do
	local art = df.artifact_record.find(id)
	local have_image = false

	for _,imp in ipairs(art.item.improvements) do
		if imp:getType() == df.improvement_type.ART_IMAGE then
			have_image = true
			break
		end
	end

	if have_image then
		-- get textviewer description of item
		local itemscreen = df.viewscreen_itemst:new()
		itemscreen.item = art.item
		dfhack.screen.show(itemscreen)

		itemscreen:feed_key(df.interface_key.ITEM_DESCRIPTION)

		local text = dfhack.df2utf(itemscreen.child.title)
		for _,s in ipairs(itemscreen.child.src_text) do
			text = text .. format_text(s.value)
		end

		-- a little bit of extra text cleanup
		text = text:gsub("  +", " ")
		text = text:gsub("\n\n\n+", "\n\n")
		text = text:gsub(" \n", "\n")
		text = text:trim()

		output:write(text, "\000")

		itemscreen.child:feed_key(df.interface_key.LEAVESCREEN)
		itemscreen:feed_key(df.interface_key.LEAVESCREEN)
	end
end

output:close()

screen:feed_key(df.interface_key.LEAVESCREEN)

screen = dfhack.gui.getCurViewscreen(true)
while not df.viewscreen_titlest:is_instance(screen) do
	screen:logic()
	screen = dfhack.gui.getCurViewscreen(true)
end

screen:feed_key(df.interface_key.STANDARDSCROLL_UP)
screen:feed_key(df.interface_key.SELECT)
