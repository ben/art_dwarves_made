module git.lubar.me/ben/art_dwarves_made

go 1.17

require (
	github.com/creack/pty v1.1.15
	github.com/mattn/go-mastodon v0.0.5-0.20210629151305-d39c10ba5e94
)

require (
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80 // indirect
)
