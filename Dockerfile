FROM golang:1.17 as builder

COPY . /src/art_dwarves_made

RUN cd /src/art_dwarves_made \
 && CGO_ENABLED=0 go build -o /art_dwarves_made

FROM benlubar/dwarffortress:dfhack-0.47.05-r2

COPY --from=builder /art_dwarves_made /usr/local/bin/art_dwarves_made

COPY dfart.lua /df_linux/hack/scripts/

ENTRYPOINT ["art_dwarves_made"]
